module gitlab.com/jaytaph/rockymqtt

go 1.13

require (
	github.com/davecgh/go-spew v1.1.1
	github.com/eclipse/paho.mqtt.golang v1.3.0
	gopkg.in/yaml.v2 v2.4.0
)
