package main

import (
	"crypto/tls"
	"crypto/x509"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"math/rand"
	"os"
	"time"

	mqtt "github.com/eclipse/paho.mqtt.golang"
	"gopkg.in/yaml.v2"
)

type Config struct {
	Broker     string `yaml:"broker"`
	Topics     []string `yaml:"topics"`
	Ca         string `yaml:"ca"`
	ClientCert string `yaml:"client_cert"`
	ClientKey  string `yaml:"client_key"`
	Debug      bool   `yaml:"debug"`
	Hooks      []struct {
		Type string `yaml:"type"`
		On   string `yaml:"on"`
		Url  string `yaml:"url"`
		Cmd string `yaml:"cmd"`
	} `yaml:"hooks"`
}

func NewConfig(p string) (*Config, error) {
	config := &Config{}

	f, err := os.Open(p)
	if err != nil {
		return nil, err
	}
	defer func() {
		_ = f.Close()
	}()

	d := yaml.NewDecoder(f)

	if err := d.Decode(&config); err != nil {
		return nil, err
	}

	return config, nil
}

var f mqtt.MessageHandler = func(client mqtt.Client, msg mqtt.Message) {
	fmt.Printf(">>> TOPIC: %s\n", msg.Topic())
	fmt.Printf(">>> MSG: %s\n", msg.Payload())
}

func NewTLSConfig(caFile, certFile, keyFile string) *tls.Config {
	certpool := x509.NewCertPool()
	pemCerts, err := ioutil.ReadFile(caFile)
	if err == nil {
		certpool.AppendCertsFromPEM(pemCerts)
	}

	cert, err := tls.LoadX509KeyPair(certFile, keyFile)
	if err != nil {
		panic(err)
	}

	return &tls.Config{
		RootCAs:            certpool,
		ClientCAs:          certpool,
		ClientAuth:         tls.RequireAndVerifyClientCert,
		InsecureSkipVerify: false,
		Certificates:       []tls.Certificate{cert},
	}
}

var configPath string
var config *Config

func main() {
	flag.StringVar(&configPath, "config", "./config.yml", "path to config")
	flag.Parse()

	// Read config file
	var err error
	config, err = NewConfig(configPath)
	if err != nil {
		log.Fatal(err)
	}

	if config.Debug {
		mqtt.ERROR = log.New(os.Stdout, "[ERROR] ", 0)
		mqtt.CRITICAL = log.New(os.Stdout, "[CRIT] ", 0)
		mqtt.WARN = log.New(os.Stdout, "[WARN]  ", 0)
		// mqtt.DEBUG = log.New(os.Stdout, "[DEBUG] ", 0)
	}

	opts := mqtt.NewClientOptions()
	opts.AddBroker(config.Broker)
	opts.SetClientID(fmt.Sprintf("go-rocky-%d", rand.Uint64()))
	opts.SetKeepAlive(10 * time.Second)
	opts.SetPingTimeout(30 * time.Second)
	opts.SetDefaultPublishHandler(f)
	opts.SetTLSConfig(NewTLSConfig(config.Ca, config.ClientCert, config.ClientKey))
	opts.SetCleanSession(true)

	c := mqtt.NewClient(opts)
	if token := c.Connect(); token.Wait() && token.Error() != nil {
		panic(token.Error())
	}

	defer func() {
		c.Disconnect(250)
	}()

	for _, topic := range config.Topics {
		if token := c.Subscribe(topic, 0, f); token.Wait() && token.Error() != nil {
			panic(token.Error())
		}
	}

	for {
		time.Sleep(5 * time.Second)
	}

	// mosquitto_sub -d --cafile centos-server-ca.cert --cert centos.cert --key centos.cert -h mqtt.git.centos.org -p 8883 -t git.centos.org/# -v
}
